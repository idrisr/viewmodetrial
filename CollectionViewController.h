//
//  CollectionViewController.h
//  ViewModeTrial
//
//  Created by idris on 6/24/15.
//  Copyright (c) 2015 Idris Raja. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewModeStore.h"

@interface CollectionViewController : UICollectionViewController
@property (strong, nonatomic) ViewModeStore *viewModeStore;
@end
