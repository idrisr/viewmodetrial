//
// Created by idris on 6/24/15.
// Copyright (c) 2015 Idris Raja. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ViewModeStore : NSObject
@property NSArray *modes;

-(instancetype) init;
@end