# View Modes

## Was having issues with the proper dislay of an image, ViewModes seem related

see this [commit](https://bitbucket.org/ayeoohs/fbknowthyself/commits/496a166454c2bdd65e430a4a3bb5ae4750dbf7e5)

### Challenge - create an app with a TableViewController that displays the same image in each of the different avaiable view modes

### 1) Different view-modes

ModeScaleToFill
ModeScaleAspectFit
ModeScaleAspectFill
ModeRedraw
ModeCenter
ModeTop
ModeBottom
ModeLeft
ModeRight
ModeTopLeft
ModeTopRight
ModeBottomLeft
ModeBottomRight

### 2) Add view modes to a model object

### 3) Display the view modes in the table view cells

### 4) Add Nav Controller to Storyboard

### 5) Add detailed ImageView to table cells

### 6) add name of View Mode as title in the detailed view

1. name the segue
1. add title and title Label to `DetailViewController`
1. implement `prepareForSeque:sender` in nav controller
1. add an image in the UIView. Does not need to be fetched -- can be local

### 6.5) Debug `viewWithTag`

1. can it work with something other than a `UILabel`?
    no
1. find 5 sources about `viewWithTag`

### 8) get rid of the master detail view and use a collection to show all the choices at once

branch `collview`

###9) add labels for the different viewmodes

### 10) Add a border

### 11) Add app icon

### 12 ) Fix it so that when rotated it looks right

1. find 5 sources about rotating the `view`
    1. [SO 1](http://stackoverflow.com/questions/18708407/how-to-rotate-a-uicollectionview-similar-to-the-photos-app-and-keep-the-current)
    2. [SO 2](http://stackoverflow.com/questions/13556554/change-uicollectionviewcell-size-on-different-device-orientations)
    3. [SO 3](http://stackoverflow.com/questions/18988028/how-do-i-define-the-size-of-a-collectionview-on-rotate)
    4. [Dash](file:///Users/idris/Library/Developer/Shared/Documentation/DocSets/com.apple.adc.documentation.iOS.docset/Contents/Resources/Documents/documentation/WindowsViews/Conceptual/CollectionViewPGforIOS/Introduction/Introduction.html)
    5. [Blog](http://forums.xamarin.com/discussion/809/uicollectionview-changing-flow-layout-for-screen-rotation)
    6. [ray w](http://www.raywenderlich.com/forums/viewtopic.php?f=2&t=5483)

### 13) Subclass `UICollectionViewFlowLayout` and implement `- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds`

### 13.5) `frames` vs `bounds`

see this [image](http://i.stack.imgur.com/bjt7r.jpg) from [this](http://stackoverflow.com/questions/8496762/nsviews-bounds-vs-frame) question

### 14) What *is* the `UICollectionViewFlowLayout`?

5 sources about `UICollectionViewFlowLayout`

1. [Dash](file:///Users/idris/Library/Developer/Shared/Documentation/DocSets/com.apple.adc.documentation.iOS.docset/Contents/Resources/Documents/documentation/UIKit/Reference/UICollectionViewLayout_class/index.html#//apple_ref/occ/cl/UICollectionViewLayout)
1. [Maniac Dev](https://maniacdev.com/2013/02/example-using-uicollectionviewflowlayout-to-easily-create-great-looking-scrollable-grid-layouts)
1. [Blog](http://www.brianjcoleman.com/tutorial-collection-views-using-flow-layout/)
1. [NSScreenCast](http://www.nsscreencast.com/episodes/46-fun-with-uicollectionview)
1. [Github example](https://github.com/AshFurrow/UICollectionViewFlowLayoutExample) 

80/20 the above sources