//
//  CollectionViewController.m
//  ViewModeTrial
//
//  Created by idris on 6/24/15.
//  Copyright (c) 2015 Idris Raja. All rights reserved.
//

#import "CollectionViewController.h"

@interface CollectionViewController ()

@end

@implementation CollectionViewController

static NSString * const reuseIdentifier = @"cell";

- (void)viewDidLoad {
    [super viewDidLoad];
    self.viewModeStore = [[ViewModeStore alloc] init];

    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = NO;

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section {
    return 13;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {

    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier
                                                                           forIndexPath:indexPath];
    // create a label programatically
    UILabel *myLabel = (UILabel *)[cell viewWithTag:102];
    myLabel.text = [NSString stringWithFormat:@"%@", @(indexPath.item)];
    myLabel.text = [self.viewModeStore.modes objectAtIndex:indexPath.item];
    myLabel.textColor= [UIColor colorWithRed:(225/255.0)
                                       green:(240/255.0)
                                        blue:(0/255.0)
                                       alpha:1] ;

    // Configure the cell
    UIImage *backgroundImage = [UIImage imageNamed:@"The-Old-Guitarist.jpg"];
    cell.backgroundView = [[UIImageView alloc] initWithImage:backgroundImage];
    cell.backgroundView.contentMode = indexPath.item;
    cell.clipsToBounds = YES;
    cell.layer.borderColor = [UIColor colorWithRed:(20  / 255.0)
                                             green:(57  / 255.0)
                                              blue:(204 / 255.0)
                                             alpha:1].CGColor;
    cell.layer.borderWidth = 1;
    return cell;
};

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger squareSize = 182;
    return CGSizeMake(squareSize, squareSize);
}

//-(BOOL)shouldAutorotate {
//    METHOD_LOG
//    return YES;
//}

//-(void)viewWillTransitionToSize:(CGSize)size
//      withTransitionCoordinator:(id <UIViewControllerTransitionCoordinator>)coordinator {
//    [self.collectionView.collectionViewLayout invalidateLayout];
//    [self.collectionView reloadData];
//}

- (void) willTransitionToTraitCollection:(UITraitCollection *)newCollection
               withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    METHOD_LOG

    [super willTransitionToTraitCollection:newCollection
                 withTransitionCoordinator:coordinator];

    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        [self.collectionView.collectionViewLayout invalidateLayout];
    } completion:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        [self.collectionView reloadData];
    }];

}

@end
