//
//  myCollectionViewFlowLayout.m
//
//
//  Created by idris on 6/25/15.
//
//

#import "myCollectionViewFlowLayout.h"

@implementation myCollectionViewFlowLayout

- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds {
    CGRect oldBounds = self.collectionView.bounds;
    if (CGRectGetWidth(newBounds) != CGRectGetWidth(oldBounds)) {
        return YES;
    }
    return NO;
}

@end