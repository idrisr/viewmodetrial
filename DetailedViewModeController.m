//
// Created by idris on 6/24/15.
// Copyright (c) 2015 Idris Raja. All rights reserved.
//

#import "DetailedViewModeController.h"


@implementation DetailedViewModeController {

}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.image.image = [UIImage imageNamed:@"The-Old-Guitarist.jpg"];
    self.image.contentMode = self.contentViewMode;

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;

    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end