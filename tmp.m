- (UITableViewCell *)tableView:(UITableView *)tableView
  cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString* PlaceholderCellIdentifier = @"PlaceholderCell";
    static const int DateLabelTag = 1;

    UITableViewCell* cell = [tableView
dequeueReusableCellWithIdentifier:PlaceholderCellIdentifier];
    if (!cell) {
        cell = [[[UITableViewCell alloc]
initWithStyle:UITableViewCellStyleSubtitle
reuseIdentifier:PlaceholderCellIdentifier] autorelease];   

        UILabel *theDateLabel = [[UILabel alloc] initWithFrame:CGRectMake(140,
35,140, 20)];
        theDateLabel.tag = DateLabelTag;
        theDateLabel.backgroundColor = [UIColor clearColor];
        theDateLabel.textColor = [UIColor lightGrayColor];
        theDateLabel.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:15];
        [cell.contentView addSubview:theDateLabel];
        [theDateLabel release];
    }

    NSDate *theDate = [self dateForRowAtIndexPath:indexPath];
    UILabel *theDateLabel = [cell.contentView viewWithTag:DateLabelTag];
    theDateLabel.text = [dateFormatter stringFromDate:theDate];

    return cell;
}
