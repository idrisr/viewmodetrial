//
// Created by idris on 6/24/15.
// Copyright (c) 2015 Idris Raja. All rights reserved.
//

#import "ViewModeStore.h"

// should be a singleton, perhaps?

@implementation ViewModeStore {

}

-(instancetype) init {
    self = [super init];

    if (self) {
        self.modes = @[
                @"ModeScaleToFill",
                @"ModeScaleAspectFit",
                @"ModeScaleAspectFill",
                @"ModeRedraw",
                @"ModeCenter",
                @"ModeTop",
                @"ModeBottom",
                @"ModeLeft",
                @"ModeRight",
                @"ModeTopLeft",
                @"ModeTopRight",
                @"ModeBottomLeft",
                @"ModeBottomRight"];
    }
    return self;
}

@end