//
//  AppDelegate.h
//  ViewModeTrial
//
//  Created by idris on 6/23/15.
//  Copyright (c) 2015 Idris Raja. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

