//
//  ViewModeTableViewController.m
//  ViewModeTrial
//
//  Created by idris on 6/23/15.
//  Copyright (c) 2015 Idris Raja. All rights reserved.
//

#import "ViewModeTableViewController.h"
#import "DetailedViewModeController.h"
#import "ViewModeStore.h"

@interface ViewModeTableViewController ()
@property ViewModeStore *viewModeStore;
@end

@implementation ViewModeTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.viewModeStore = [[ViewModeStore alloc] init];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.

    return [self.viewModeStore.modes count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    static NSString *cellIdentifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier
                                                            forIndexPath:indexPath];
    cell.textLabel.text = [self.viewModeStore.modes objectAtIndex:indexPath.row];

    return cell;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue
                sender:(id)sender {
    if ([segue.identifier isEqualToString:@"detailViewSegue"]){
        DetailedViewModeController *detailedViewModeController = (DetailedViewModeController*)segue.destinationViewController;
        NSIndexPath *indexPath  = [self.tableView indexPathForCell:(UITableViewCell *)sender];
        detailedViewModeController.title = [self.viewModeStore.modes objectAtIndex:indexPath.row];
        detailedViewModeController.contentViewMode = indexPath.row;
    }
}

@end
